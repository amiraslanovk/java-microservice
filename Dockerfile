# Use an appropriate base image
FROM maven:3.8.4-openjdk-11-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the pom.xml and src files to the container
COPY pom.xml .
COPY src ./src

# Build the Maven project
RUN mvn package

# Set the entrypoint command to run the application
CMD ["java", "-jar", "target/my-maven-project-1.0-SNAPSHOT.jar"]